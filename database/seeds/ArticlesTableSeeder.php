<?php

use App\Article;
use App\User;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::truncate();
        $userIds=User::pluck('id');

        foreach ($userIds as $userId){
            factory(Article::class,3)->create(['user_id'=>$userId]);
        }


    }
}
