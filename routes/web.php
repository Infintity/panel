<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();



Route::get('/panel', 'HomeController@index')->name('home');

Route::prefix('article')->group(function (){

    Route::get('/',"ArticleController@index")->name('articles');
    Route::get('/create','ArticleController@create')->name('create-article');
    Route::post('/store','ArticleController@store')->name('store-article');
    Route::get('/edit/{article}','ArticleController@edit')->name('edit-article');
    Route::put('/update/{article}','ArticleController@update')->name('update-article');
    Route::match(['delete','get'],'/delete/{article}','ArticleController@destroy')->name('delete-article');


});

Route::prefix('users')->group(function (){

    Route::get('/','UserController@index')->name('users');
    Route::get('/profile/{user}','UserController@profile')->name('profile');
    Route::put('/update/{user}','UserController@update')->name('update-user');
    Route::put('/update-about-user/{user}','UserController@updateAboutUser')->name('update-about-user');
    Route::put('/update-user-cover/{user}','UserController@updateUserCover')->name('update-user-cover');
    Route::put('/update-user-avatar/{user}','UserController@updateUserAvatar')->name('update-user-avatar');
    Route::match(['delete','get'],"delete/{user}","UserController@destroy")->name('delete-user');

});

Route::get("/panel/search","HomeController@search")->name('search');




