@extends('layouts.panel.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <form method="post" action="{{route('update-article',[$article->id])}}">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="firstName1">عنوان</label>
                                <input type="text" name="title" class="form-control" id="firstName1"
                                       placeholder="عنوان مقاله را وارد کنید" value="{{$article->title}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="lastName1">متن مقاله</label>
                                <textarea name="body" rows="7" class="form-control" >{{$article->body}}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary">تایید</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
