@extends('layouts.panel.master')


@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-header d-flex align-items-center border-0">
                    <h3 class="w-50 float-left card-title m-0">لیست مقالات</h3>
                    <div class="dropdown dropleft text-right w-50 float-right">
                        <button class="btn bg-gray-100" type="button" id="dropdownMenuButton1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="nav-icon i-Gear-2"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <a class="dropdown-item" href="{{route('create-article')}}">افزودن مقاله جدید</a>
                        </div>
                    </div>
                </div>

                <div class="">
                    <div class="table-responsive">
                        <table id="user_table" class="table  text-center">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">عنوان</th>
                                @if(auth()->user()->isAdmin())
                                    <th scope="col">ارسال شده توسط</th>
                                @endif()
                                <th scope="col">تاریخ ارسال</th>
                                <th scope="col">اقدام</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                @can('view',$article)
                                    <tr>
                                        <th scope="row">{{$loop->index+1}}</th>
                                        <td>{{$article->title}}</td>
                                        @if(auth()->user()->isAdmin())
                                            <td>
                                                {{$article->user()->value('name')}}
                                            </td>
                                        @endif

                                        <td>{{$article->created_at}}</td>
                                        <td>
                                            <a id="edit" href="{{route('edit-article',[$article->id])}}"
                                               class="text-success mr-2">
                                                <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                                            </a>
                                            <a id="delete-item" href="{{route('delete-article',[$article->id])}}"
                                               class="text-danger mr-2">
                                                <i class="nav-icon i-Close-Window font-weight-bold"></i>
                                            </a>

                                        </td>
                                    </tr>
                                @endcan
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            @if(request()->route()->getName()=='search' and request()->exists('q'))
                {{$articles->appends(['q'=>request()->get('q')]) -> links('partials.pagination')}}
            @else
                {{$articles->links('partials.pagination')}}
            @endif


        </div>
    </div>




@endsection
