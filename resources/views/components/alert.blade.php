
<div class="alert alert-card alert-{{$type}}" role="alert">
    <strong class="text-capitalize">{{$title }}</strong>{{$message}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>

