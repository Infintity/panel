@extends('layouts.panel.master')


@section('content')


    {{--new users--}}

    @can('view',\App\User::class)
        <div class="row">
            <div class="col-md-12">
                <div class="card o-hidden mb-4">
                    <div class="card-header d-flex align-items-center border-0">
                        <h3 class="w-50 float-left card-title m-0">کاربران جدید</h3>
                        <div class="dropdown dropleft text-right w-50 float-right">
                            <button class="btn bg-gray-100" type="button" id="dropdownMenuButton1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="nav-icon i-Gear-2"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <a class="dropdown-item" href="{{route('users')}}">مشاهده تمام کاربران</a>
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <div class="table-responsive">
                            <table id="user_table" class="table  text-center">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">نام</th>
                                    <th scope="col">آواتار</th>
                                    <th scope="col">ایمیل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $article)
                                    <tr>
                                        <th scope="row">{{$loop->index+1}}</th>
                                        <td>{{$article->user()->value('name')}}</td>
                                        <td>

                                            <img class="rounded-circle m-0 avatar-sm-table "
                                                 src="{{$article->user()->value('avatar')? asset('storage/avatars/'."/".$article->user()->value('avatar')) : asset('assets/images/faces/9.jpg')}}" alt="">

                                        </td>

                                        <td>{{$article->user()->value('email')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endcan()

    {{--new user end--}}

    {{--new articles--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-header d-flex align-items-center border-0">
                    <h3 class="w-50 float-left card-title m-0">لیست مقالات مقالات اخیر</h3>
                    <div class="dropdown dropleft text-right w-50 float-right">
                        <button class="btn bg-gray-100" type="button" id="dropdownMenuButton1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="nav-icon i-Gear-2"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <a class="dropdown-item" href="{{route('create-article')}}">افزودن مقاله جدید</a>
                            <a class="dropdown-item" href="{{route('articles')}}">مشاهده تمام مقالات</a>
                        </div>
                    </div>
                </div>

                <div class="">
                    <div class="table-responsive">
                        <table id="user_table" class="table  text-center">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">عنوان</th>
                                @if(auth()->user()->isAdmin())
                                    <th scope="col">ارسال شده توسط</th>
                                @endif
                                <th scope="col">تاریخ ارسال</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                @can('view',$article)
                                    <tr>
                                        <th scope="row">{{$loop->index+1}}</th>
                                        <td>{{$article->title}}</td>
                                        @if(auth()->user()->isAdmin())
                                            <td>
                                                {{$article->user()->value('name')}}
                                            </td>
                                        @endif

                                        <td>{{$article->created_at}}</td>
                                    </tr>
                                @endcan
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>


    {{--new articles end--}}
@endsection
