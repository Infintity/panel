@include('layouts.panel.header')
<div class="app-admin-wrap">
{{--content header--}}
@include('partials.content-header')

@include('layouts.panel.sidebar')


<!-- ============ Body content start ============= -->
    <div class="main-content-wrap sidenav-open d-flex flex-column">

        @if(session('status')==='success')
            @component('components.alert')
                @slot('type','success')
                @slot('title','موفق ')
                @slot('message','تغیرات با موفقیت اعمال شد')
            @endcomponent
        @endif
        @yield('content')




        {{--content-footer--}}
        @include('partials.content-footer')








@include('layouts.panel.footer')
