<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{request()->route()->getName()=="home"?'active':''}} " data-item="">

                <a class="nav-item-hold" href="{{route('home')}}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">داشبورد</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{request()->route()->getName()=="articles"?'active':''}}">
                <a class="nav-item-hold" href="{{route('articles')}}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="nav-text">مقالات</span>
                </a>
                <div class="triangle"></div>
            </li>
            @can('view')
            <li class="nav-item {{request()->route()->getName()=="users"?'active':''}} " >
                <a class="nav-item-hold" href="{{route('users')}}">
                    <i class="nav-icon i-Administrator"></i>
                    <span class="nav-text">کاربران</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endcan
        </ul>
    </div>



</div>
