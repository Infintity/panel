@if($paginator->hasPages())
    <div class="card text-left">
        <div class="card-body">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    @if($paginator->onFirstPage())
                        <li class="page-item disabled"><a class="page-link" href="#">قبلی</a></li>
                    @else
                        <li class="page-item "><a class="page-link" href="{{$paginator->previousPageUrl()}}">قبلی</a>
                        </li>
                    @endif

                    @foreach($elements as $element)
                        @foreach($element as $page=>$url)
                            @if($page==$paginator->currentPage())
                                <li class="page-item active"><a class="page-link" href="#">{{$page}}</a></li>
                            @else
                                <li class="page-item "><a class="page-link" href="{{$url}}">{{$page}}</a></li>
                            @endif
                        @endforeach
                    @endforeach

                    @if($paginator->hasMorePages())
                        <li class="page-item"><a class="page-link" href="{{$paginator->nextPageUrl()}}">بعدی</a>
                        </li>
                    @else
                        <li class="page-item"><a class="page-link" href="#">بعدی</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

@endif
