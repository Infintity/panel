<div class="main-header">
    <div class="logo">
        <img src="{{asset('assets/images/logo.png')}}" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
        <form action="{{route('search')}}" id="search-form">
            <div class="search-bar">
                <input type="text" name="q" placeholder="جستجو">
                <i class="search-icon text-muted i-Magnifi-Glass1"></i>
            </div>
        </form>
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>


        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
                <img src="{{auth()->user()->avatar?asset("storage/avatars")."/".auth()->user()->avatar:asset('assets/images/faces/9.jpg')}}" id="userDropdown" alt="" data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{auth()->user()->name}}
                    </div>
                    <a href="{{route('profile',[auth()->user()->id])}}" class="dropdown-item">تنظیمات حساب</a>
                    <a class="dropdown-item" id="logout-btn">خروج</a>

                </div>
            </div>
        </div>
    </div>
    <form id="logout-form" method="post" action="{{route('logout')}}">
        @csrf
    </form>
    <script>

        $('#logout-btn').on('click', function (event) {
            event.preventDefault();
            $('#logout-form').submit()
        })

    </script>

</div>
