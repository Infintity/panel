<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer">
    <div class="row">
        <div class="col-md-9">
            <p><strong>الماس - قالب داشبورد مدیریت بوت استرپ 4</strong></p>
            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
            </p>
        </div>
    </div>
    <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
        <a class="btn btn-primary text-white btn-rounded" href="https://themeforest.net/user/mh_rafi" target="_blank">خرید قالب الماس</a>
        <span class="flex-grow-1"></span>
        <div class="d-flex align-items-center">
            <img class="logo" src="assets/images/logo.png" alt="">
            <div>
                <p class="m-0">&copy; 2018 قالب الماس</p>
                <p class="m-0">همه حقوق محفوظ است</p>
            </div>
        </div>
    </div>
</div>
<!-- footer end -->
</div>
<!-- ============ Body content End ============= -->
</div>
<!--=============== End app-admin-wrap ================-->

