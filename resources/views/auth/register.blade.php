@extends('layouts.master')

@section('content')
    <div class="auth-layout-wrap" style="background-image: url({{asset('assets/images/photo-wide-4.jpg')}})">
        <div class="auth-content">
            <div class="card o-hidden">
                <div class="row">
                    <div class="col-md-6 text-center"
                         style="background-size: cover;background-image: url({{asset('assets/images/photo-wide-4.jpg')}})">
                        <div class="pl-3 auth-right">
                            <div class="auth-logo text-center mt-4">
                                <img src="assets/images/logo.png" alt="">
                            </div>
                            <div class="flex-grow-1"></div>
                            <div class="w-100 mb-4">
                                <a class="btn btn-outline-primary btn-outline-email btn-block btn-icon-text btn-rounded"
                                   href="{{route('register')}}">
                                    <i class="i-Mail-with-At-Sign"></i> ورود با ایمیل
                                </a>
                                <a class="btn btn-outline-primary btn-outline-google btn-block btn-icon-text btn-rounded">
                                    <i class="i-Google-Plus"></i> ورود با گوگل
                                </a>
                                <a class="btn btn-outline-primary btn-outline-facebook btn-block btn-icon-text btn-rounded">
                                    <i class="i-Facebook-2"></i> ورود با فیس بوک
                                </a>
                            </div>
                            <div class="flex-grow-1"></div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="p-4">

                            <h1 class="mb-3 text-18">ثبت نام</h1>
                            <form action="{{route('register')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="username">نام شما</label>
                                    <input id="username" value="{{old('name')}}" name="name"
                                           class="form-control form-control-rounded @error('name') {{'is-invalid'}}  @enderror "
                                           type="text">
                                </div>
                                <div class="form-group">
                                    <label for="email">آدرس ایمیل</label>
                                    <input id="email" name="email"
                                           class="form-control form-control-rounded @error('email') {{'is-invalid'}}  @enderror"
                                           type="email" value="{{old('email')}}">
                                </div>
                                <div class="form-group">
                                    <label for="password">رمزعبور</label>
                                    <input id="password" name="password"
                                           class="form-control form-control-rounded @error('password') {{'is-invalid'}}  @enderror"
                                           type="password">
                                </div>
                                <div class="form-group">
                                    <label for="repassword">تکرار رمزعبور</label>
                                    <input id="repassword" name="password_confirmation"
                                           class="form-control form-control-rounded @error('password_confirmation') {{'is-invalid'}}  @enderror"
                                           type="password">
                                </div>
                                <button class="btn btn-primary btn-block btn-rounded mt-3">ثبت نام</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
