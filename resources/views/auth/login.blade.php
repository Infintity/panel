@extends('layouts.master')

@section('content')
    <div class="auth-layout-wrap" style="background-image: url({{asset('assets/images/photo-wide-4.jpg')}})">

        <div class="auth-content">
           <div class="row justify-content-center">
               <div class="col-6 text-center">
                   @if($errors->any())
                       @foreach($errors->all() as $error)
                           @component('components.alert')
                               @slot('type','danger')
                               @slot('title','خطا ')
                               @slot('message',$error)
                           @endcomponent
                       @endforeach
                   @endif
               </div>

           </div>
            <div class="card o-hidden">
                <div class="row">
                    <div class="col-md-6">
                        <div class="p-4">
                            <div class="auth-logo text-center mb-4">
                                <img src="{{asset('assets/images/logo.png')}}" alt="">
                            </div>
                            <h1 class="mb-3 text-18">ورود</h1>
                            <form method="post" action="{{asset('login')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="email">آدرس ایمیل</label>
                                    <input id="email" name="email" class="form-control form-control-rounded" type="email"
                                           value="{{old('email')}}">
                                </div>
                                <div class="form-group">
                                    <label for="password">رمزعبور</label>
                                    <input id="password" name="password" class="form-control form-control-rounded" type="password">
                                </div>
                                <button class="btn btn-rounded btn-primary btn-block mt-2">ورود</button>

                            </form>

                            <div class="mt-3 text-center">
                                <a href="forgot.html" class="text-muted"><u>رمز عبور را فراموش کرده اید؟</u></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center"
                         style="background-size: cover;background-image: url({{asset('assets/images/photo-long-3.jpg')}})">
                        <div class="pr-3 auth-right">
                            <a class="btn btn-rounded btn-outline-primary btn-outline-email btn-block btn-icon-text"
                               href="{{route('register')}}">
                                <i class="i-Mail-with-At-Sign"></i> ثبت نام با ایمیل
                            </a>
                            <a class="btn btn-rounded btn-outline-primary btn-outline-google btn-block btn-icon-text">
                                <i class="i-Google-Plus"></i> ثبت نام با گوگل
                            </a>
                            <a class="btn btn-rounded btn-outline-primary btn-block btn-icon-text btn-outline-facebook">
                                <i class="i-Facebook-2"></i> ثبت نام با فیس بوک
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
