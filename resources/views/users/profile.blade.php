@extends('layouts.panel.master')

@section('content')

    <div class="main-content-wrap sidenav-open d-flex flex-column">

        <div class="card user-profile o-hidden mb-4">
            <div class="header-cover"
                 style="background-image: url({{$user->cover_image?asset('storage/avatars')."/".$user->cover_image:asset('assets/images/photo-wide-1.jpg')}})"></div>
            <div class="user-info">
                <img class="profile-picture avatar-lg mb-2"
                     src="{{$user->avatar?asset('storage/avatars')."/".$user->avatar:asset('assets/images/faces//9.jpg')}}"
                     alt="">
                <p class="m-0 text-24">{{$user->name}}</p>
            </div>
            <div class="user-info">

                @if($errors->about->any())
                    @foreach($errors->about->all() as $error)
                        @component('components.alert')
                            @slot('type','danger')
                            @slot('title','خطا ')
                            @slot('message',$error)
                        @endcomponent
                    @endforeach
                @endif

                <div class="tab-content" style="width: 80%" id="profileTabContent">

                    <h4>اطلاعات شخصی</h4>
                    <p>{{$user->about}}
                    </p>
                    <div>
                        <button type="button" class="btn btn-primary m-1" data-toggle="modal"
                                data-target="#about-me-modal"
                                data-whatever="@mdo">ویرایش
                        </button>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Calendar text-16 mr-1"></i> تاریخ تولد</p>
                                <span>1 دی 1374</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Edit-Map text-16 mr-1"></i> محل تولد</p>
                                <span>تهران</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Globe text-16 mr-1"></i> زندگی می کند در</p>
                                <span>تهران</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> جنسیت</p>
                                <span>مرد</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> ایمیل</p>
                                <span>example@ui-lib.com</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Cloud-Weather text-16 mr-1"></i> وبسایت</p>
                                <span>www.example.com</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Face-Style-4 text-16 mr-1"></i> حرفه</p>
                                <span>بازاریابی دیجیتال</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Professor text-16 mr-1"></i>تجربه</p>
                                <span>8 سال</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Home1 text-16 mr-1"></i> مدرسه</p>
                                <span>دانشکده بازاریابی دیجیتال</span>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div>
                        @if($errors->cover->any())
                            @foreach($errors->cover->all() as $error)
                                @component('components.alert')
                                    @slot('type','danger')
                                    @slot('title','خطا ')
                                    @slot('message',$error)
                                @endcomponent
                            @endforeach
                        @endif
                        <h4 class="py-1">تغیر عکس کاور</h4>
                        <form action="{{route('update-user-cover',$user->id)}}" class="form-group" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="input-group mb-3">
                                <div class="custom-file">

                                    <input name="cover" type="file" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">انتخاب فایل</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary m-1">
                                ارسال
                            </button>

                        </form>
                    </div>
                    <hr>
                    <div>
                        @if($errors->avatar->any())
                            @foreach($errors->avatar->all() as $error)
                                @component('components.alert')
                                    @slot('type','danger')
                                    @slot('title','خطا ')
                                    @slot('message',$error)
                                @endcomponent
                            @endforeach
                        @endif
                        <h4 class="py-1">تغیر عکس پروفایل</h4>
                        <form action="{{route('update-user-avatar',$user->id)}}" class="form-group" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="input-group mb-3">
                                <div class="custom-file">

                                    <input name="avatar" type="file" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">انتخاب فایل</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary m-1">
                                ارسال
                            </button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>



    {{-- change cover photo moadl --}}
    <div class="modal fade" id="cover-modal" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyModalContent_title">درباره کاربر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="about-user-from" method="post" action="{{route('update-about-user',auth()->user()->id)}}">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="message-text-1" class="col-form-label">درباره من</label>
                            <textarea rows="4" name="about" class="form-control" id="message-text-1"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <button id="about-user-from-btn" type="button" class="btn btn-primary">ارسال پیام</button>
                </div>
            </div>
        </div>
    </div>


@endsection
