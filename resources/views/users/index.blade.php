@extends('layouts.panel.master')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-header d-flex align-items-center border-0">
                    <h3 class="w-50 float-left card-title m-0">لیست کاربران</h3>
                    <div class="dropdown dropleft text-right w-50 float-right">
                        <button class="btn bg-gray-100" type="button" id="dropdownMenuButton1" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="nav-icon i-Gear-2"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <a class="dropdown-item" href="{{route('create-article')}}">افزودن مقاله جدید</a>
                        </div>
                    </div>
                </div>

                <div class="">
                    <div class="table-responsive">
                        <table id="user_table" class="table  text-center">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام</th>
                                <th scope="col">اواتار</th>
                                <th scope="col">ایمیل</th>
                                <th scope="col">تاریخ عضویت</th>
                                <th scope="col">اقدام</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                @can('view',$user)
                                    <tr>
                                        <th scope="row">{{$loop->index+1}}</th>
                                        <td>{{$user->name}}</td>
                                        <td>
                                            <img class="rounded-circle m-0 avatar-sm-table "
                                                 src=" {{$user->avatar?asset('storage/avatars')."/".$user->avatar:asset('assets/images/faces//9.jpg')}}"
                                                 alt="">

                                        </td>

                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>
                                            @can('view-profile',$user)
                                                <a href="{{route('profile',[$user->id])}}"
                                                   class="text-success mr-2">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                                                </a>
                                            @endcan
                                            @can('delete',$user)
                                                <a id="delete-item" href="{{route('delete-user',[$user->id])}}"
                                                   class="text-danger mr-2">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>
                                                </a>
                                            @endcan

                                        </td>
                                    </tr>
                                @endcan
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{$users->links('partials.pagination')}}

        </div>
    </div>
@endsection
