<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $table='articles';

    protected $with=['user'];


    protected $fillable=['user_id','title','slug','body'];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
