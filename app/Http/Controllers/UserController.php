<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view',User::class);
        $users=User::orderBy('created_at','Desc')->paginate(15);

        return  view('users.index',compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
       //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update',auth()->user());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete',$user);
        $user->delete();
        return redirect()->route('users')->with(['status'=>'success']);
    }



    public function updateAboutUser(Request $request, User $user){
        $this->authorize('view-profile',$user);
        $validate=Validator::make($request->all(),[
            'about' =>['min:20','max:512','nullable']
        ]);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate,'about');
        }
        $user->about=$request->about;
        $user->save();
        return redirect()->route('profile',$user->id)->with('status','success');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function updateUserCover(Request $request, User $user)
    {


        $this->authorize('view-profile',$user);
        $validate=Validator::make($request->all(),[
            'cover'=>['required','mimes:jpg,png,jpeg']
        ]);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate,'cover');
        }
        [$fileName,$fileExtension]=["cover-".$user->id,$request->file('cover')->extension()];
        $request->file('cover')->storeAs('avatars',$fileName.".".$fileExtension);
        $user->cover_image=$fileName.".".$fileExtension;
        $user->save();
        return redirect()->route('profile',$user->id)->with('status','success');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateUserAvatar(User $user, Request $request){

        $this->authorize('view-profile',$user);

        $validate=Validator::make($request->all(),[
            'avatar'=>['required','mimes:jpg,png,jpeg']
        ]);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate,'avatar');
        }
        [$fileName,$fileExtension]=["avatar-".$user->id,$request->file('avatar')->extension()];
        $request->file('avatar')->storeAs('avatars',$fileName.".".$fileExtension);
        $user->avatar=$fileName.".".$fileExtension;
        $user->save();
        return redirect()->route('profile',$user->id)->with('status','success');


    }

    public function profile(User $user){
        $this->authorize('view-profile',$user);
        return view('users.profile',compact('user'));
    }
}
