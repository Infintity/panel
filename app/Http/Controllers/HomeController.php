<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if (auth()->user()->isAdmin())
            $articles = Article::orderBy('id', 'DESC')->limit(5)->get();
        else
            $articles = Article::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->limit(5)->get();
        return view('panel.index', compact('articles'));
    }

    public function search(Request $request)
    {

        if ($request->filled('q')) {
            if (auth()->user()->isAdmin())
                $articles = Article::where('body', 'like', "%$request->q%")->orWhere('title', 'like', "%$request->q%")->paginate(15);
            else
                $articles = Article::where('user_id', auth()->user()->id)->where(function ($query) use ($request) {
                    $query->where('title', 'like', "%$request->q%")->orWhere('body', 'like', "%$request->q%");
                })->paginate(15);
            return view('articles.index', compact('articles'));
        }

        return redirect()->route('home');


    }
}
