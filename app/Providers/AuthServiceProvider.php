<?php

namespace App\Providers;

use App\Article;
use App\Policies\ArticlePolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Article::class => ArticlePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability, $args) {
            if ($user->isAdmin()) {

                if (isset($args[0]) and $args[0] instanceof User) {
                    switch ($ability) {
                        case 'delete': return true;
                        case 'view':return true;
                        case 'view-profile':return true;

                    }
                }
                else return true;
            }
        });
    }
}
