<?php

namespace App\Policies;

use App\Article;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user , Article $article){
        return $user->id===$article->user_id;
    }

    public function create(User $user){
        return true;
    }

    public function update(User $user,Article $article){
        return $user->id==$article->user_id;
    }

    public function delete(User $user , Article $article){
        return $user->id==$article->user_id;
    }

}
