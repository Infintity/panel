$(function () {



    $("a#delete-item").on("click", function (event) {
        event.preventDefault();
        let url=this.getAttribute('href');
        swal({
            title: "شما مطمئن هستید؟",
            text: "شما نمیتوانید این را بازخوانی کنید!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0cc27e",
            cancelButtonColor: "#FF586B",
            confirmButtonText: "بله، آن را حذف کنید!",
            cancelButtonText: "نه لغو کنید!",
            confirmButtonClass: "btn btn-success mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then(function () {

            window.location=url;
        }, function (t) {
            "cancel" === t && swal("لغو شد", "عملیات لغو شد", "error")
            event.preventDefault();
        })
    })

})


$('#search-form').find('i.search-icon').on('click',function () {
    $(this).parent().parent().submit()
})


$('#about-user-from-btn').click(function (event) {
    $('#about-user-from').submit()
});


CKEDITOR.replace('body',{
    'contentsLangDirection' : 'rtl',
});
